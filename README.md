# node10-gulp-chromium
Karma, Jasmine and Protractor with Chromium in a docker container.

Unfortunately, chromium doesn't support container, you need argument: `--no-sandbox`

[Coderi : Inteligência Empresarial](http://www.coderi.com.br) | [DockerHub](https://cloud.docker.com/repository/registry-1.docker.io/kamihouse/node10-gulp-chromium) custom image.

Workdir: `/app`  
Chromium: `/usr/bin/chromium`

Example: `docker run --net=host -v $(pwd):/app -w /app -it kamihouse/node10-gulp-chromium bash`

### Sample service

```
protractor:
  image: kamihouse/node10-gulp-chromium
  script:
    - npm run test
```

### Utilities

protractor.config.js

```
exports.config = {
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      'args': ['no-sandbox', 'headless', 'disable-gpu']
    }
  }
};

```
